#! /usr/bin/env python
from MuonTPPostProcessing.Defs import *
from MuonTPPostProcessing.FileUtils import ReadInputConfig
from MuonTPPostProcessing.CheckMetaData import GetNormalizationDB, GetWeightHandler
from MuonTPPostProcessing.CalculateLumiFromIlumicalc import CalculateRecordedLumi
import os

name = DSConfigName(Name="ZIso_2023")

#############################################################
# User configuration Part

# Paths of my samples and input confs
paths = {
    # "samples": "/afs/cern.ch/work/s/sangelid/private/mcp/testrun/OUTPUT/2024-01-07/myProcessJob_2023_v2/",
    "samples": "/afs/cern.ch/work/w/wema/public/MuonTPPostProcessing_Data23/run/ZIso_2023/Sample/",
    # "samples": "/afs/cern.ch/work/w/wema/public/MuonTPPostProcessing_Data23/run/ZIso_2023/OUTPUT/2024-03-27/ZIso_2023/",
    "inputCfgs": "/afs/cern.ch/work/w/wema/public/MuonTPPostProcessing_Data23/run/InputConf/2023"
}

# Specify if this is run3 (important to load the right XS file)
isRun3 = True

# Specify luminosity (0 will start luminosity calculation)
Lumi = 27.8  # for 2023
# Lumi = 25.76699710033438

#############################################################
# Gather data histogram files (outputs of SubmitToBatch/WriteTagProbeHistos)
DataSamples = [S for S in os.listdir(
    paths["samples"]) if S.startswith("data_2023") and "UNKNOWN" not in S]

#############################################################
# Luminosity Determination (loads the ilumicalc defined in ClusterSubmission/data/GRL.json)
# We can set Lumi by hand if we already know it, or if we don't care to have it precicely on the plots.
# (Efficiency plots are not affected by luminosity; it can be anything > 0)

if Lumi == 0:
    print('\n*********************************')
    print(f'INITIATING LUMINOSITY DETERMINATION\n')

    # Gather the input data NTUPs by parsing the input configs for luminosity calculation
    # (normally input config = name of DataSample with ".root" --> ".conf")
    DataFiles = []
    for S in DataSamples:
        DataFiles += ReadInputConfig("%s/%s.conf" %
                                     (paths["inputCfgs"], S[:-5]))

    for R in GetNormalizationDB(DataFiles, False, isRun3).GetRunNumbers():
        # if GetWeightHandler().getTriggerHelper().is_2k23(R):
        runLumi = CalculateRecordedLumi(R)
        print(f'Adding luminosity for RunNumber {R} = {runLumi}')
        Lumi += runLumi

    if Lumi == 0:
        print(f'\nERROR: Determined luminosity = 0; cannot proceed.')
        exit()

    Lumi /= 1.e3

    print(f'\nSETTING DATA LUMINOSITY TO: {Lumi}')
    print('*********************************\n')

#############################################################
# DSconfigs for each process to consider
# Check also /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc23.txt

# DSconfig for the data
Data = DSconfig(name="Data_2023",
                filepath=["%s/%s" % (paths["samples"], S)
                          for S in DataSamples],
                lumi=Lumi,
                label="Data 2023",
                # Assume 2% lumi uncertainty (what is it for 2023?)
                relScalingUncert=0.02,
                sampletype=SampleTypes.Data)

# DSconfig for the Z+jets signal
Zmumu = DSconfig(name="Zmumu",
                 filepath=paths["samples"] + "2023_Zmumu_r14799_67.root",
                 color=ROOT.kBlue - 6,
                 label="Z#rightarrow#mu#mu",
                 relScalingUncert=0.04,
                 sampletype=SampleTypes.Signal)

Zmumu_Sherpa = DSconfig(name="Zmumu_Sherpa",
                        filepath=paths["samples"] +
                        "2023_Sherpa2214_Zmumu_r14799_67.root",
                        color=ROOT.kCyan - 6,
                        relScalingUncert=0.04,
                        label="Z#rightarrow#mu#mu (Sherpa)",
                        sampletype=SampleTypes.Signal,
                        associateWithSys=Systematics.isoGen)

# DSconfig for Ztautau
Ztautau = DSconfig(name="Ztautau",
                   filepath=paths["samples"] + "2023_Ztautau_r14799_67.root",
                   color=ROOT.kSpring - 1,
                   relScalingUncert=0.04,
                   label="Z#rightarrow#tau#tau",
                   sampletype=SampleTypes.Signal)

# DSconfig for ttbar
TTbar = DSconfig(name="TTbar",
                 filepath=paths["samples"] + "2023_TTbar_r14799_67.root",
                 # relScalingUncert = 0.03,
                 color=ROOT.kOrange - 2,
                 label="t#bar{t} (dilep)",
                 sampletype=SampleTypes.Signal)

# DSconfig for W+jets
Wjets = DSconfig(name="Wjets",
                 filepath=paths["samples"] + "2023_Wjets_r14799_67.root",
                 color=ROOT.kGreen + 1,
                 label="W #rightarrow#mu#nu",
                 sampletype=SampleTypes.Reducible)  # Get from fit
